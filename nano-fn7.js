cl=console.log;

function reduce(arr,a=0){
	if(typeof arr=="undefined"){
		return;
	}else if(arr.length==0){
		return a;
	}else{
		var a=a+arr[arr.length-1];
		var rest=arr;rest.pop();
		return reduce(rest, a);
 	}
}

cl(reduce([1,2,3,4,5,6,7,8,9]));

function reducee(arr,a=0,i=0){
	if(typeof arr=="undefined"){
		return;
	}else if(i>arr.length-1){
		return a;
	}else{
		return reducee(arr,a+arr[i],i+1);
	}
}

cl(reducee([0,1,2,3,4,5,6,7,8,9]));

function numm(arr,i=0,r=[]){
	if(typeof arr=="undefined"){
		return;
	}else if(typeof arr[i+1]!=="undefined"){
		r[i]=arr[i]+arr[i+1];
		return numm(arr,i+1,r);
	}else{
		return r;
	}
}

cl(numm([0,1,2]))

// 1,2,3 ÷ 3,5 ÷ 8
// 1,2,3,4 ÷ 3,5,7 ÷ 8,12 ÷ 20
// for loop style
var se1=[1,2,3,4],len1=se1.length;;

for(let i=0;i<len1-1;i++){
  se1=numm(se1);
}
cl(se1);

//(functional) recursive style
function pair(arr){
	if(arr.length>1){
		pair(numm(arr));
	}else{
		return arr[0];
	}
}
cl(pair(se1));

var tree1=[[0],[[1]]];

function find(tree,key,i=0){
	var type=typeof key,stack=[],t=tree;
	function cd(tree,i=0){
	}
	return cd(tree);
}
cl(find(tree1,0));
cl(find(tree1,1));

function rsv(arr,idx=0,i=0){
	console.log("rsv",arr,i);
	if(Array.isArray(arr)){//if array(folder)
		if(arr.length>1){
			scan(arr);
			// list all folders at 1 i
		}else{
			rsv(arr[idx],idx+1,i+1);
		}
	}else{
		return; //if value(file)
	}
}

//rsv([[[]]])

function scan(arr,i=0){
	cl("scan",i);
	if(typeof arr[i]=="undefined"){
		return;
	}else{
		rsv(arr[i]);
		scan(arr,i+1);
	}
}
//scan([[],[],[[1,[]]]])
//combined scan and rsv..

function list(arr,i=0,r=[]){
	if(Array.isArray(arr[i])){
		r.push({i,type:"folder",length: arr[i].length});
	}else{
		r.push({i, type:"file", value:arr[i]});
	}
	if(typeof arr[i+1]!=="undefined"){
		list(arr,i+1,r);
	}
	return r;
}
cl(list([1,2,3,[]]));
