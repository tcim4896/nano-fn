cl=console.log;

function reduce(arr,a=0,i=0){
  if(typeof arr=="undefined"){
    return;
  }else if(i==arr.length-1){
    return a;
  }else{
    return reduce(arr,a+arr[i],i+1);      
  }
}                                              

cl(reduce([1,2,3,4,5,6,7,8,9]));

function numm(arr,i=0,r=[]){
    if(typeof arr=="undefined"){
        return;
    }else if(typeof arr[i+1]!=="undefined"){
        r[i]=arr[i]+arr[i+1];
        return numm(arr,i+1,r);
    }else{
        return r;
    }
}

cl(numm([0,1,2,3,4]));