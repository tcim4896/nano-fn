function rsv(arr,idx=0,i=0){
	cl(1,arr)
	if(Array.isArray(arr)){
		if(arr.length>0){
			scan(arr);
		}else{
			rsv(arr[idx],idx+1,i+1);
		}
	}else{
		return;
	}
}
// rsv(layout1);
// rsv into node.children
function scan(arr,i=0){
	tree1.push(arr);
	if(typeof arr[i+1]!=="undefined"){
		rsv(arr[i+1])
		scan(arr,i+1)
	}else{
		return;
	}
}
scan(template1);
console.log(tree1);

function list(arr,i=0,r=[]){
	if(Array.isArray(arr[i])){
		r.push('arr', arr[i])
	}else{
		r.push('val',arr[i])
	}
	if(typeof arr[i+1]!=="undefined"){
		list(arr,i+1,r);
	}
	return r;
}
// Attempt structure rebuild(copy)
function scan(arr, i=0, r=[]){
  console.log(i);
  if(typeof arr[i]=="object"){
    if(Array.isArray(arr[i])){
      console.log("ArrayHandle")
      r.push(scan(arr[i]))
    }else{
      console.log("ObjectHandle")
	  console.log(1,arr[i])
      r.push(iterationCopy(arr[i])
    }
  }else if(typeof arr[i]!=="undefined"){
    r.push(arr[i]);
  }
  
  if(typeof arr[i+1]!=="undefined"){
    scan(arr,i+1,r);
  }
  function iterationCopy(src) {
    let target = {};
    for (let prop in src) {
        if (src.hasOwnProperty(prop)) {
			if(typeof src[prop]=="object"){
    			if(Array.isArray(src[prop])){
					console.log("ArrayHandle",src[prop])
        			target[prop] = scan(src[prop]);
				}else{
      				console.log("ObjectHandle", src[prop])
					target[prop] = iterationCopy(src[prop]);
				}
			}else{
				target[prop] = src[prop];
			}
        }
    }
    return target;
  }
  return r;
}
var node1 =[1,{a:1,b:{c:1}}];
console.log(scan(node1));
var node2 = scan(node1);
node2[1].b.c=2
console.log(node1);
// full rebuild of object succeeded

// building dom tree options
window.mStateMap={};
Object.prototype.add=function(...sub){
	var self=this;
  if(!self.sub)
  	self.sub=[];
  sub.forEach(sub=>{
  	self.sub.push(sub);
  })
  return self;
}
var n1={text:"n1"};
function select(id){
	return window.mStateMap[id];
}

//def(10)("i"+i)
//def("list1",list)
//o.level1.add(o.list1)
//where o refers to mStateMap
//level.add(list1)
//every object(node) coverts into dom
//{text:"euj"}=<div>euj<div>
//shorthand code and wildcards

for(let i=0;i<10;i++){
	window
  .mStateMap["i"+i]={text:"i"+i};
  n1.add(select("i"+i));
}
var i1={text:"i1"};
n1.add(i1);
console.log(n1);
/*
 * n1(i1,i2,i3)
 * n2(n3(n4))
 *
 * */
