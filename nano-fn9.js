/*
 * for every unit the sequence
 * for example
 * 10=918273645546372819
 * also tuples
 * [(9,1),(8,2)..]
 * */

function unitEvery(unit,tuple/*bool*/){
	var r=[],tuples;
	for(let i=1;i<=unit-1;(i++)){
			r.push([i,unit-i])	
	}
	return tuple?r:r.map(tuple=>tuple.join('')).join('');
}
console.log(unitEvery(10,true))
console.log(unitEvery(10,false))
console.log((1,2,3));//get last

//function declaration returns undefined
