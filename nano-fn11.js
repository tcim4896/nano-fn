cl=console.log;

function bitscan(bits,source){
  cl("SCANNING")
  var r=[],
      blen=bits.length,
      slen=source.length;
  for(let a=0;a<slen;a++){
    var match=true;
    for(let b=0;b<blen;b++){
      cl(bits[b],source[a+b],match)
      if(bits[b]!==source[a+b]){
        match=false;
      }
      if(b==blen-1&&match==true){
        cl("MATCH","index:",a)
        r.push(["index:",a])
      }
    }
  }
  return r;
}

function filter(chars,source){
  var r=[],
      blen=chars.length,
      slen=source.length,
      output="";
  for(let a=0;a<slen;a++){
    var match=true;
    for(let b=0;b<blen;b++){
      if(chars[b]!==source[a+b]){
        match=false;
      }
      if(b==blen-1&&match==true){
        r.push({a,blen})
      }      
    }
  }
  let exclude=[];
  for(let data of r){
    for(let i=0;i<data.a+data.blen-1;i++){
      exclude.push(data.a+i)
    }
  }
  
  output=source
    .split('')
    .filter(function(char,idx){
    cl(idx, exclude.indexOf(idx))
      return exclude.indexOf(idx)==-1;
  }).join('');
  cl(output)
  return output;
}

filter("234","123 4567")