var cl = console.log;

var o1 = {
    vars: {
        distance: 100,
        height: 30
    },
    get distance() {
        return this.vars.distance;
    },
}
///////
// https://stackoverflow.com/questions/4616202/

var foo = {
    a: 5,
    b: 6,
    init: function() {
        this.c = this.a + this.b;
        return this;
    }
 }.init();

 var foo = function(o) {
    o.c = o.a + o.b;
    return o;
}({a: 5, b: 6});

///////

function fnnr(object) {
    return object.distance;
}

cl(fnnr(o1));

function inverseValue(value) {
    return String(value).split('').map((val, index)=>{
        return val * Math.pow(10, index);
    }).reduce((a,b)=>a+b, 0);
}

cl(inverseValue(3752));

function luciferbox() {
    function box() {
        var state = 0;
        function getState(state){
            return ['closed', 'opening', 'open', 'closing'][state];
        }
    }

    function lucifer() {
        var state = 0;

        
    }
}

cl((()=>{
    return 1+2;
})());

function suspicion(){
    return Promise;
}