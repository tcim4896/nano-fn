c=console;cl=console.log;
// let ejs = require('ejs'),
//     users = ['geddy', 'neil', 'alex'];

//console.dir((ejs.render('<ul><?= users.join(" | "); ?></ul>', {users: users},{delimiter: '?'})));

function nnSimple(){
	
}

function oSqrt(val){
	return (Math.sqrt(val)/val)*(Math.sqrt(val)/val);
}
// 25 = 5 / 5 = 25
cl(oSqrt(5));

function slideOpen(arr){
	var r;
	for(let i=0;i<arr.length;i++){
		setTimeout(function(x){
			cl(arr[i]);
		},500*i)
	}
}

function m(iterator,condition,fn){
	condition=>{return arguments;}
	cl(arguments);
	
}
m(10,{i:10},a=>cl(a))

// handler parameter object
var oObj1={
	iterator:10,
	condition: {i:0},
	callback: function(i){
		return i;
	}
}
// object handler
function oo(oObj){
	let r=[],
	i=oObj.iterator, 
	x=oObj.condition.i,
	cb=oObj.callback;
	for(;i>x;i--){
		r.push(cb(i));
	}
	return r;
}
cl(oo(oObj1));
/*
(i=0;i<10;i++)
(i=10;i>0;i--)*/
//slideOpen([0,2,4,5,1,3]);

var fnObj={
	0: function(p1){
		return p1;
	},
	1: function(p1,p2){
		return {a:p1,b:p2};
	}
};
//function array
var fnArr1=[
	fnObj[0], 
	fnObj[1]
];
//function array handler
function d2Receive(functionArray){
	var r={},i=0;
	for(let fn of functionArray){
		r[i]=fn(i);
		i+=1;
	}
	return r;
}

cl(d2Receive(fnArr1));
// high and low freq osc
function hfLf(h,l,c){
	for(let i=0; i<c;i++){
		setTimeout(function(){
			return cl(i);
		}, i*(i%h===0?h:l))
	}
}

// hfLf(50,100,300);

// beat pattern
var beatPtrn = [..."100010222100301"];
// samples
var samples = {
	0: "0",
	1: "kick",
	2: "hihat",
	3: "snare"
}
// run
for(let id of beatPtrn){
	cl(samples[id])
}

function closeScreen(){
	var rr=[Array(10), Array(10)];
	cl(rr);
	for(let x=0,y=10-1; x<10; (x++,y--)){
		([rr[0][x]=0,rr[1][y]=0])
		cl(rr[0],[rr[1]]);
	}
}

cl(closeScreen());

function map(array, fn){
	return [].map.apply(array,[fn]);
}

function map(array,fn){
    var rr=array;
    for(let i=0;i<rr.length;i++){
        rr[i]=fn(rr[i]);
    }
    return rr;
}

cl(map([1,2,3,4],(x)=>x*4));
// time stuff
function timeArray(array){
	var rr=array;
	function timeValue(delay){
		var r=0, t=[];
		for(let i=0;i<500;i++){ 
			t.push(setTimeout(function(){
				i=i==3?0:i;
				r=i;
			},i*delay));
		}
		return ()=>{
			t.forEach(delay=>clearTimeout(delay));
			return r;
		};
	}

	function decide(fnArray, id){
		var rr=fnArray;
		for(let i=0;i<rr.length;i++){
			rr[i]=rr[i]();
		}
		return rr;
	}

	for(let i=0;i<rr.length;i++){
		// fill array with loops
		rr[i]=timeValue(array[i]);
	}
    // hlprs
	
	return (fn)=>{
		return typeof fn === "function"?
			fn(rr):
			decide(rr);
	};
}

var fd = timeArray([5,3,2,1]);

setTimeout(function(x=0){
	cl(fd());
}, 1003);
// time stuff;;

var _3d1={
    0: [0,1,2,
        3,4,5,
        6,7,8],
    1: [0,1,2,
        3,4,5,
        6,7,8],
    2: [0,1,2,
        3,4,5,
        6,7,8,],
};

var _3d2=`
0,1,2,
3,4,5,
6,7,8,

9,10,11,
12,13,14,
15,16,17,

18,19,20,
21,22,23,
24,25,26,
`;

cl(Array.from(Array(10).keys()))

function step(matrix, direction, num){
	var r;
	switch(direction){
		case 'z':
			r=Math.ceil(matrix/3)*num;
		break;
		case 'x':

		break;
		case 'y':

		break;
		default: console.log("No direction given..");
	}
}